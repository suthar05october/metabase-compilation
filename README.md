# Cheat Sheet for metabase compilation Ubuntu 16.04
# Install ngnix 

```
apt -y install software-properties-common
apt -y install nginx
sudo nano /etc/nginx/sites-available/default
server_name dev.dap-udea.net; #(Note:-you need to add # Virtual Host configuration for example.com after this line you need to add server name)
sudo nginx -t
sudo systemctl reload nginx


#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )


```
# Install Mysql Server

- Download MySQL Server. Enter a root password when specified:

```
sudo apt install mysql-server

```
# Ngnix is installed then go to hml folder by typing command
```
cd /var/www/html/
```
# Take a clone from github 
```
git clone https://github.com/metabase/metabase.git

```
#Metabase folder is created now go to metabase folder
```
 cd metabase/
```

##  Installing Java

- Install software-properties-common to easily add new repositories
```
sudo apt-get install software-properties-common

```
no need to add
- Baisc Infomation
-this java command will show
-Do you want to continue (y/N)
-please press y


- Add the Java PPA:

```
sudo add-apt-repository ppa:webupd8team/java 

```

- Update the source list

```

sudo apt-get update

```
- Install the Java JDK 8:
```
 #sudo apt-get install oracle-java8-installer--not use
 #sudo apt-get install oracle-java12-set-default---not use
 sudo apt-get install openjdk-8* ---use this now
 update-alternatives --config java ---output ---JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"
 nano /etc/environment
 JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"
 source /etc/environment
 echo $JAVA_HOME
```
- Baisc Infomation
-then it will show two popup on terminal
-first you need to press ok
-second you need to press yes

# Installing node js latest version
```
 sudo apt install curl
 curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
 sudo apt-get install -y nodejs
```
# Install Yarn
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```
# Install leiningen
```
wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
chmod +x lein
sudo mv lein /usr/local/bin
lein -v
```
#By default metabase run on localhost so changing the port
  ``` 
    export MB_JETTY_HOST=dev.dap-udea.net
  ```
# login to ftp for editing the file it has same as ssh details if you don't have filezilla please install on your system
```
host:159.203.163.157
user:root
password:(xxxxxxxxxxxx)
port:22
```

# when you login through ftp edit webpack.config.js(194-195)
```
config.output.publicPath =
    "hhttp://dev.dap-udea.net:8080/" + config.output.publicPath;
```
# Another line edit number is (217-232)
```
    config.devServer = {
    hot: true,
    inline: true,
    contentBase: "frontend",
    host: 'dev.dap-udea.net',
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    // if webpack doesn't reload UI after code change in development
    // watchOptions: {
    //     aggregateTimeout: 300,
    //     poll: 1000
    // }
    // if you want to reduce stats noise
    // stats: 'minimal' // values: none, errors-only, minimal, normal, verbose
  };


# we are changing metabase name to gitcbase edit this file through ftp /var/www/html/metabase/frontend/src/metabase/auth/container/loginApp.jsx line number is (128)
```
<h3 className="Login-header Form-offset">{t`Sign in to Gitcbase`}</h3>
```

# Changing public dashbaord footer text /var/www/html/metabase/frontend/src/metabase/public/components/LogoBadge.jsx line number is(20-23)#change ttitle in dashboard public to gticbase and href for powered by for https://gtic.com.co/
```
    const LogoBadge = ({ dark }: Props) => (
  <a
    href="https://gtic.com.co/"
    target="_blank"
    className="h4 flex text-bold align-center no-decoration"
  >
    <LogoIcon size={28} dark={dark} />
    <span className="text-small">
      <span className="ml1 text-medium">Powered by</span>{" "}
      <span className={cx({ "text-brand": !dark }, { "text-white": dark })}>
        GitcBase
      </span>
    </span>
  </a>
);
```
# change color in metabase /var/www/html/metabase/frontend/src/metabase/css/core/colors.css (If you want to change the color you need to search the color code in this file and replace with your color)
```
   :root {
  --color-brand: #e14b0f;
  --color-accent1: #9cc177;
  --color-accent2: #e14b0f;
  --color-accent3: #ef8c8c;
  --color-accent4: #e14b0f;
  --color-accent5: #f2a86f;
  --color-accent6: #98d9d9;
  --color-accent7: #7172ad;
  --color-admin-navbar: #7172ad;
  --color-white: #ffffff;
  --color-black: #2e353b;
  --color-success: #84bb4c;
  --color-error: #ed6e6e;
  --color-warning: #f9cf48;
  --color-text-dark: #2e353b;
  --color-text-medium: #74838f;
  --color-text-light: #c7cfd4;
  --color-text-white: #ffffff;
  --color-bg-black: #2e353b;
  --color-bg-dark: #93a1ab;
  --color-bg-medium: #edf2f5;
  --color-bg-light: #f9fbfc;
  --color-bg-white: #ffffff;
  --color-shadow: rgba(0, 0, 0, 0.08);
  --color-border: #d7dbde;

  /* night mode colors */
  --night-mode-color: color(var(--color-text-white) alpha(-14%));
  --night-mode-card: #42484e;
}
```
# change color in metabase /var/www/html/metabase/frontend/src/metabase/lib/colors.js (If you want to change the color you need to search the color code in this file and replace with your color)
```
const colors = {
  brand: "#e14b0f",
  accent1: "#9CC177",
  accent2: "#e14b0f",
  accent3: "#EF8C8C",
  accent4: "#e14b0f",
  accent5: "#F2A86F",
  accent6: "#98D9D9",
  accent7: "#7172AD",
  "admin-navbar": "#7172AD",
  white: "#FFFFFF",
  black: "#2E353B",
  success: "#84BB4C",
  error: "#ED6E6E",
  warning: "#F9CF48",
  "text-dark": "#2E353B",
  "text-medium": "#74838F",
  "text-light": "#C7CFD4",
  "text-white": "#FFFFFF",
  "bg-black": "#2E353B",
  "bg-dark": "#93A1AB",
  "bg-medium": "#EDF2F5",
  "bg-light": "#F9FBFC",
  "bg-white": "#FFFFFF",
  shadow: "rgba(0,0,0,0.08)",
  border: "#D7DBDE",
};
```
#Custom map not view in shared Dashboards(/var/www/html/metabase/src/metabase/api/routes.clj) replace this text in line number:70
```
(context "/geojson"              [] geojson/routes)
```

# change the metabase logo(/var/www/html/metabase/frontend/src/metabase/components/logoicon.jsx)
```
export default class LogoIcon extends Component {
  static defaultProps = {
    size: 48, ---you need replace logo size
  };

  static propTypes = {
    size: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
    dark: PropTypes.bool,
  };

  render() {
    let { dark, height, width, size } = this.props;
    return (
      <svg
        className={cx("Icon", { "text-brand": !dark }, { "text-white": dark })}
        viewBox="0 0 66 85"
        width={width || size}
        height={height || size}
        fill="black" ---here you can replace the logo color you edit color js and css file mentioned above
      >
         ---you need add your svg code here and i already added my customized code
        <g id="surface1">
          <path d="M 42.199219 22.101563 L 25.898438 5.800781 C 25.398438 5.300781 24.699219 5 24 5 C 23.300781 5 22.601563 5.300781 22.101563 5.800781 L 18.601563 9.300781 L 22.699219 13.398438 C 23.101563 13.199219 23.5 13.101563 24 13.101563 C 25.699219 13.101563 27 14.398438 27 16.101563 C 27 16.601563 26.898438 17 26.699219 17.398438 L 30.699219 21.398438 C 31.101563 21.199219 31.5 21.101563 32 21.101563 C 33.699219 21.101563 35 22.398438 35 24.101563 C 35 25.800781 33.699219 27.101563 32 27.101563 C 30.300781 27.101563 29 25.800781 29 24.101563 C 29 23.601563 29.101563 23.199219 29.300781 22.800781 L 25.300781 18.800781 C 25.199219 18.800781 25.101563 18.898438 25 18.898438 L 25 29.300781 C 26.199219 29.699219 27 30.800781 27 32.101563 C 27 33.800781 25.699219 35.101563 24 35.101563 C 22.300781 35.101563 21 33.800781 21 32.101563 C 21 30.800781 21.800781 29.699219 23 29.300781 L 23 18.800781 C 21.800781 18.398438 21 17.300781 21 16 C 21 15.5 21.101563 15.101563 21.300781 14.699219 L 17.199219 10.601563 L 5.800781 22.101563 C 5.300781 22.601563 5 23.300781 5 24 C 5 24.699219 5.300781 25.398438 5.800781 25.898438 L 22.101563 42.199219 C 22.601563 42.699219 23.300781 43 24 43 C 24.699219 43 25.398438 42.699219 25.898438 42.199219 L 42.199219 25.898438 C 42.699219 25.398438 43 24.699219 43 24 C 43 23.300781 42.699219 22.601563 42.199219 22.101563 Z "/>
         </g>
      </svg>
    );
  }
}
```
# change favicon in metabase need to replace your favicon here /var/www/html/metabase/resources/frontend_client

#Edit in admin UI Metabase tittle left up side and right up side in menu #About to Acerca de
#Metabase to Gticbase #hide this "Metabase es una marca registrada de Metabase, Incy está construido con cariño en San Francisco, CA"
```
/var/www/html/metabase/frontend/src/metabase/nav/components/ProfileLink.jsx
```
#In login UI favicon and tittle
# When i share into whatsapp change tittle replace these files
#in pages like share dashboard tittle still metabase
# change metabase admin to gitcbase admin
```
/var/www/html/metabase/resources/frontend_client/index_template.html
/var/www/html/metabase/resources/frontend_client/index.html
/var/www/html/metabase/resources/frontend_client/init.html
/var/www/html/metabase/resources/frontend_client/public.html
var/www/html/metabase/resources/frontend_client/embed.html
/var/www/html/metabase/frontend/src/metabase/hoc/Title.jsx
/var/www/html/metabase/frontend/src/metabase/routes.jsx
/var/www/html/metabase/resources/frontend_client/app/dist/app-embed.bundle.js
/var/www/html/metabase/frontend/src/metabase/nav/containers/Navbar.jsx

```
#For change the metabase to gitcbase on welcome page
```
  /var/www/html/metabase/frontend/src/metabase/setup/components/Setup.jsx

```

# Translate to spanish or any language

```
  bin/i18n/update-translation es.po
  bin/i18n/update-translation your_locale

```
#how change a string with po file, i use poeditor but how compile that .mo and .po file
  ```
    Edit the locales/es.po change the string
    ./bin/i18n/build-translation-resources
    
  ```
#Now creating build of metabase jar
  ```
    apt-get update
    apt-get install gettext
  ./bin/build no-translations

  ```
#Now go to cd /target/uberjar then run metabase jar
  ```
  java -jar metabase.jar &

  ```
#Ngnix setting for reverse proxy to work without port or with ssl
```
cd /etc/ngnix/sites-available
```
```
touch ngnix.conf
```
```
nano ngnix.conf
```
#add these lines

```
server {
listen dev.dap-udea.net:443;
server_name dev.dap-udea.net;
return 301 http://dev.dap-udea.net:3000; }
```
#(for nano ctrl-o then ctrl-m and then ctrl-x it will save and exit the file)
```
cd /etc/ngnix/sites-enabled
```
```
touch dev.dap-udea.net.conf
nano dev.dap-udea.net.conf
```
#add this line 
```
server
{
 listen 443 ssl;
 server_name dev.dap-udea.net www.dev.dap-udea.net;
 location /
 {
 proxy_set_header Host $http_host;
 proxy_set_header X-Forwarded-Host $host;
 proxy_set_header X-Forwarded-Server $host;
 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; proxy_set_header X-Graylog-Server-URL https://domain.com/api;
 proxy_pass http://dev.dap-udea.net:3000;
 # proxy_pass http://dev.dap-udea.net:3000;
 }
 ssl on;
 ssl_certificate /etc/letsencrypt/live/dev.dap-udea.net/fullchain.pem;
 ssl_certificate_key /etc/letsencrypt/live/dev.dap-udea.net/privkey.pem;
 ssl_session_timeout 5m;
 ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;
 ssl_protocols TLSv1.2;
 ssl_prefer_server_ciphers on;
 access_log /var/log/nginx/dev.dap-udea.net.access.log;
 error_log /var/log/nginx/dev.dap-udea.net.error.log;
}

# http to https redirection
server {
    listen 80;
    server_name dev.dap-udea.net www.dev.dap-udea.net;
    add_header Strict-Transport-Security max-age=2592000;
    rewrite ^ https://$server_name$request_uri? permanent;
}

```

```
systemctl reload nginx
java -jar metabase.jar (# check if its working propelry then use nohup othewise not)
```

#For permanent run the jar file without opening any command using nohup
  ```
   nohup java -jar metabase.jar &

  ```







